/*!
 *  Copyright (c) 2021, Rahul Gupta and Headright Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
var os = require('os');
var linebreakStyle = os.EOL === '\n' ? 'unix' : 'windows';

module.exports = {
  root: true,
  env: {
    node: true,
    es2021: true,
    mocha: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2021,
  },
  rules: {
    // Platform Dependent
    'linebreak-style': ['error', linebreakStyle], // allows CRLF checkout on windows

    // Sensible Defaults
    'semi-style': 'off', // allows semantically meaningful empty line semicolon
    'function-paren-newline': ['error', 'consistent'], // easier to manage long argument lists
    'no-use-before-define': ['error', { 'functions': false }],
    'no-unused-vars': ['error', { 'argsIgnorePattern': 'reject' }], // allow re
    'no-return-await': 'off', // do not swallow functions in error stacks

    // Personal Taste
    'brace-style': ['error', 'stroustrup', { 'allowSingleLine': true }],
    'no-else-return': 'off',
    'no-lonely-if': 'off',
    'wrap-iife': ['error', 'inside'],
    'no-multiple-empty-lines': ['error', { max: 2 }],
    'prefer-arrow-callback': 'off',
    'operator-linebreak': ['error', 'after', { overrides: { '?': 'after', ':': 'after' } }],
    'object-curly-newline': ['error', { multiline: true, consistent: true }],
    'arrow-parens': ['error', 'as-needed', { requireForBlockBody: true }],

    // Import Settings
    'no-multi-spaces': ['error', {
      exceptions: { 'ImportDeclaration': true }, // allows aligned input statements
    }],
    'import/no-extraneous-dependencies': ['error', {
      'devDependencies': [ '!@rushstack/eslint-patch' ],
    }],
    'import/no-unresolved': [ 'error', {
      ignore: ['fs/promises'],
    }],
    'import/extensions': ['error', 'ignorePackages'],

    // CLI Settings
    'no-console': 'off', // cli-apps-need-to-log
    'prefer-template': 'off', // allow string arithmetic for template proxy
  },
};
