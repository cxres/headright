const settings = Object.freeze({
  /* Header Notice */
  notice: {
    /* Header Format
       Properties not used need not be specified in these settings */
    template:
`Copyright (c) $<year>, $<author>

$<license>

SPDX-License-Identifier: $<spdx>`,

    /* Query pattern for template string
       Since we do not use a templating engine */
    queryPattern: /\$<.*?>/g,

    /* Author of commit files
       (If not specifies will be queried directly from local git configuration) */
    // author: '',

    /* Contact Information of Commit File Author
       (If not specified will be queried directly from local git configuration) */
    // contact: '',

    /* Custom strings
       Properties starting with pkg are retrieved from package.json at cwd */
    custom: {
      /* Add your own custom property */
      // property1: '',
    },

    /* License */
    license: {
      /* License Header
         (Preferred over header file) */
      // notice: '',

      /* Location of the License header file
         (relative to cwd) */
      // location: '',
    },

    /* License SPDX identifier
       (If not specified will be obtained from package.json file at cwd) */
    // spdx: '',
  },

  /* Add Settings */
  add: {
    /* Regex to detect template */
    detect: /Copyright \(c\) (?:\d{4}|-|, )*.*/,

    /* Regex for File Prefixes to preserve */
    filePrefix: {
      js: ['#!.*?', '#\\s*.*?coding=.*?', '\\/\\/\\s*@flow.*?', '\\/\\*\\s*@flow\\s*\\*\\/'],
      html: ['<!.*?'],
    },

    /* Comment style wrapping the header */
    commentStyles: {
      code: {
        blockStart: '/*!\n',
        blockEnd: '\n */',
        lineStart: ' *  ',
      },
      markup: {
        blockStart: '<!--\n',
        blockEnd: '\n-->',
        lineStart: ' ',
      },
    },

    /* Mapping file type to comment style */
    extensionStyleMap: {
      js: 'code',
      css: 'code',
      html: 'markup',
    },
  },

  /* Update Settings */
  update: {
    /* Regex to Update
       Currently, only a capturing group named "date" containing year range is processed */
    match: /Copyright \(c\) (?<date>(?:\d{4}|-|, )*(?:\d{4})), /m,
  },

  /* Files to ignore in Globstar format */
  ignore: [
    '{**,.}/node_modules/**',
  ],
});

export default settings;
