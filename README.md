# Headright

**Headright** lets you add and update headers in files. It is especially suitable for adding legal information such as copyright and license notices.

## Installation

Add **Headright** to your package as a `devDependency`:

```sh
<npm|pnpm|yarn> add -D headright
```

To use **Headright** in multiple projects, install globally using `-g` flag in lieu of the `-D` flag.

## Initialization

Initialization is automatically done during local installations. However, it can also be run later.

### CLI

*When installed locally, commands must be preceded by `npx`, `pnpx` or `yarn` as appropriate.*

Initialize **Headright** with the `hr-init` command:

```sh
hr-init
```

This creates a configuration file `.headrightrc.js` with sensible defaults. To create the configuration file with another name use the `--config` or `-c` flag.

```sh
hr-init -c hr.config.js
```

You shall be prompted before overwriting an existing configuration file. To avoid the prompt, use:
+ `--force` or `-f` flag to overwrite without asking, or
+ `--ignore` or `-i` flag to skip without asking.

Using both options together will result in an error.

### API

You can create the configuration file programmatically:

```js
import { configuration } from 'headright';

await configuration.initialize('path/to/new/config/file');
```


## Usage

Before using **Headright**, edit the configuration file as needed. All the options are explained in the comments within the configuration file itself.

### CLI

*When installed locally, commands must be preceded by `npx`, `pnpx` or `yarn` as appropriate.*

Run **Headright** from the command line using `hr` command:

```sh
hr [options] <files¹>
```

### API

To run **Headright** from a script, you need to load the configuration file and pass it to `headright` along with other options.

```js
import { configuration, headright } from 'headright';

// Load the Configuration File
const config = await configuration.load(/* optional path/to/config/file */);
// or create you own config

// Invoke Headright with minimum options
const result = await headright({
  config,
  files: ['**'],
});

// List the files modified
console.log(results.filter(result.hasChanged && result.file).join('\n');
```

### Options

+ Files (Mandatory): List of files or glob patterns to modify.\
  API: `files` Array of files/glob strings\
  CLI: `<files¹>` List of files/glob strings

  ¹*Remember to quote or escape any globs to prevent the shell from expanding them before they are passed to **Headright**.*

+ Dry Run: Report the files that shall be changed. Does not mutate files.\
  API: `dry` Boolean\
  CLI²: `-d`, `--dry`

  ²*When using `--dry` without `--verbose` on the command line, Headright will exit with `code 1` when it detects that files would be modified. This is suitable for use, for example, with git precommit hooks*

+ Add Only³: Only add new headers in files where one does not previously exist.\
  API: `addOnly` Boolean\
  CLI: `-a`, `--add-only`

+ Update Only³: Only update notices in files (where they previously exist).\
  API: `updateOnly` Boolean\
  CLI: `-u`, `--update-only`

  ³*Using both --add-only and --update-only together is the same as not specifying either, that is, both add and update will be carried out.*

### Options (CLI only)

+ Verbose: List file changes\
  CLI: `-v`, `--verbose`

+ Help: Show command line options\
  CLI: `-h`, `--help`


### Results (API)

The output is an array of objects with a list of files examined and if they were modified:

```json
[
  {
    "file": "path/to/files/file1.html",
    "hasChanged": true
  },
  {
    "file": "path/to/files/file2.html",
    "hasChanged": false
  },
]
```


## Copyright & License

Copyright © 2021, Rahul Gupta and Headright Contributors.

**Headright** is distributed under the terms of the Mozilla Public License, v. 2.0.
