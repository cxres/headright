#!/usr/bin/env node
/*!
 *  Copyright (c) 2021, Rahul Gupta and Headright Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import { stat, readdir, readFile } from 'fs/promises';
import libPath from 'path';
import minimist from 'minimist';
import prompts from 'prompts';

import { configuration } from '../../src/index.js';

const ROOT = process.env.INIT_CWD || process.cwd();

const title = `
Headright
=========
`;

async function overwriteConfig(configFile) {
  await configuration.initialize(libPath.resolve(ROOT, configFile));
}

async function promptForConfigOverwrite(configFile) {
  return await prompts({
    type: 'confirm',
    name: 'overwrite',
    message: `Overwrite config file ${configFile}?`,
    initial: false,
  });
}

function parseCli(argv) {
  return minimist(argv.slice(2), {
    string: [
      'config',
    ],
    boolean: [
      'ignore',
      'force',
      'help',
    ],
    alias: {
      c: 'config',
      f: 'force',
      i: 'ignore',
      h: 'help',
    },
    unknown: function unknownArg(x) {
      if (x[0] === '-') {
        console.warn(`Unknown argument ${x} will be ignored!`);
        return false;
      }
      return true;
    },
  });
}

function ignoreMsg(file) {
  return `Configuration File ${file} already exists. Shall not overwrite!`;
}

function overwriteMsg(file) {
  return `Configuration file written to ${file}`;
}

async function initialize(options) {
  const moduleName = 'headright';

  if (options.help) {
    const help = await readFile(new URL('./help.txt', import.meta.url), 'utf8');
    return help;
  }

  if (options.force && options.ignore) {
    throw new Error('Invalid flag combination --force & --ignore');
  }

  if (options.force) {
    if (options.config) {
      await overwriteConfig(options.config);
      return overwriteMsg(options.config);
    }
    else {
      await overwriteConfig(`.${moduleName}rc.js`);
      return overwriteMsg(`.${moduleName}rc.js`);
    }
  }

  if (options.config) {
    try {
      await stat(libPath.resolve(options.config));
      if (!options.ignore) {
        const { overwrite } = await promptForConfigOverwrite(options.config);
        if (overwrite) {
          await overwriteConfig(options.config);
          return overwriteMsg(options.config);
        }
        return '';
      }
      return ignoreMsg(options.config);
    }
    catch (error) {
      if (error.code === 'ENOENT') {
        await overwriteConfig(options.config);
        return overwriteMsg(options.config);
      }
      throw error;
    }
  }

  const rootFiles = await readdir(ROOT);
  const configFile = rootFiles.find(file => file.startsWith(`.${moduleName}rc.`) || file.startsWith(`${moduleName}.config.`));

  if (configFile) {
    if (!options.ignore) {
      const { overwrite } = await promptForConfigOverwrite(configFile);
      if (overwrite) {
        await overwriteConfig(configFile);
        return overwriteMsg(configFile);
      }
      return '';
    }
    return ignoreMsg(configFile);
  }

  await overwriteConfig(`.${moduleName}rc.js`);
  return overwriteMsg(`.${moduleName}rc.js`);
}

(async function init() {
  try {
    console.log(title);
    console.log(await initialize(parseCli(process.argv)));
  }
  catch (error) {
    console.error(error);
    process.exitCode = 1;
  }
})();
