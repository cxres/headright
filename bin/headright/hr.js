#!/usr/bin/env node
/*!
 *  Copyright (c) 2021, Rahul Gupta and Headright Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import minimist from 'minimist';
import { readFile } from 'fs/promises';

import { headright, configuration } from '../../src/index.js';
import handleResults from './results.js';

const title = `
Headright
=========
`;

function parseCli(argv) {
  return minimist(argv.slice(2), {
    boolean: [
      'add-only',
      'update-only',
      'help',
      'verbose',
      'dry',
    ],
    string: [
      'config',
    ],
    alias: {
      h: 'help',
      a: 'add-only',
      u: 'update-only',
      v: 'verbose',
      d: 'dry',
      c: 'config',
    },
    unknown: function unknownArg(x) {
      if (x[0] === '-') {
        console.warn(`Unknown argument ${x} will be ignored!`);
        return false;
      }
      return true;
    },
  });
}

async function runHeadright(options) {
  if (options.help) {
    return await readFile(new URL('./help.txt', import.meta.url), 'utf8');
  }
  if (!options._.length) {
    throw new Error('No file or glob specified.');
  }
  const only = !(options['add-only'] && options['update-only']);
  const config = await configuration.load({ file: options.config });
  const results = await headright({
    files: options._,
    addOnly: only && options['add-only'],
    updateOnly: only && options['update-only'],
    dry: options.dry,
    config,
  });
  return handleResults(results, {
    dry: options.dry,
    verbose: options.verbose,
  });
}

(async function run() {
  try {
    const result = await runHeadright(parseCli(process.argv));
    if (typeof result === 'string') {
      console.log(title);
      console.log(result);
    }
    else {
      if (result) {
        console.error('Headright: Files have outdated headers. Please Fix!');
        process.exitCode = 1;
      }
    }
  }
  catch (error) {
    console.error(error);
    process.exitCode = 1;
  }
})();
