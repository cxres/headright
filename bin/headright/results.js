/*!
 *  Copyright (c) 2021, Rahul Gupta and Headright Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
function handleResults(results, options) {
  if (options.verbose) {
    const changed = [];
    const unchanged = [];

    if (results.length === 0) {
      return 'No files touched!';
    }

    results.forEach(result => (
      result.hasChanged ? changed.push(result.file) : unchanged.push(result.file)));

    let msgChanged;
    let msgUnchanged;

    if (options.dry) {
      msgChanged = changed.length === 0 ?
        'No Files shall be modified.' :
        `Files to be modified:\n  + ${changed.join('\n  + ')}`
      ;
      msgUnchanged = unchanged.length > 0 ?
        `Files that would not be modified:\n  \u2022 ${unchanged.join('\n  \u2022 ')}` :
        ''
      ;
    }
    else {
      msgChanged = changed.length === 0 ?
        'No Files were modified.' :
        `Files modified:\n  + ${changed.join('\n  + ')}`
      ;
      msgUnchanged = unchanged.length > 0 ?
        `Files not modified:\n  \u2022 ${unchanged.join('\n  \u2022 ')}` :
        ''
      ;
    }
    return `${msgChanged}\n\n${msgUnchanged}`;
  }

  else /* (!options.verbose) */ {
    if (options.dry) {
      return (results.some(result => result.hasChanged));
    }
    else {
      return '';
    }
  }
}

export default handleResults;
