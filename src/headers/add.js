/*!
 *  Copyright (c) 2021, Rahul Gupta and Headright Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import libPath from 'path';
import { EOL } from 'os';

RegExp.escape = function regexpReplace(string) {
  return string.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');
};

function add(notice, {
  filePrefix = [],
  commentStyles = {},
  extensionStyleMap = {},
  detect,
}) {
  const headers = {};
  const notCpReList = {};

  function wrapNoticeInComment(style) {
    const {
      blockStart = '',
      blockEnd = '',
      lineStart = '',
    } = commentStyles[style] || {};

    const start = (blockStart.endsWith('\n') ? blockStart + lineStart : blockStart)
      .replace(/\r?\n/g, EOL);
    const body = notice
      .replace(/\r?\n/g, EOL + lineStart)
      .replace(new RegExp(`^${RegExp.escape(lineStart)}$`, 'gm'), lineStart.trimRight())
    ;
    const end = blockEnd.replace(/r?\n/g, EOL) + EOL;

    // save to avoid recalculation
    headers[style] = start + body + end;
    return headers[style];
  }

  function evalNotCpReItem(extn) {
    // Reject extensions for which template is not defined
    if (!Object.prototype.hasOwnProperty.call(extensionStyleMap, extn)) {
      return /(?!)/;
    }

    const fPrefix = filePrefix[extn];
    const lineEnd = '(?:\\r|\\n|\\r\\n)';

    const prefix = Array.isArray(fPrefix) && fPrefix.length >= 0 ?
      `(?:${fPrefix.join('|')}|)` : '';
    const prefixRe = prefix ? `(?:${prefix}${lineEnd})*` : '';
    const notCpMinRe = `(?!${prefixRe}\\W*${detect.source})`;
    notCpReList[extn] = new RegExp(`^${prefixRe}${notCpMinRe}`);

    // save to avoid recalculation
    return notCpReList[extn];
  }

  function notCpRe(file) {
    const extn = libPath.extname(file).substr(1);
    return notCpReList[extn] || evalNotCpReItem(extn);
  }

  function cpAdd(match, ...args) {
    const file = args.pop();
    const extn = libPath.extname(file).substr(1);
    const style = extensionStyleMap[extn];
    const header = headers[style] || wrapNoticeInComment(style);
    return match + header;
  }

  return Object.freeze({
    from: notCpRe,
    to: cpAdd,
  });
}

export default add;
