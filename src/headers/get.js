/*!
 *  Copyright (c) 2021, Rahul Gupta and Headright Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import { execSync } from 'child_process';
import { readFile } from 'fs/promises';
import { join as pathJoin } from 'path';
import { get as _get } from 'lodash-es';
import readPkg from 'read-pkg-up';

async function findReplace(str, query, replacement) {
  const matches = str.match(query);
  if (!matches) {
    return str;
  }
  const substitutes = await Promise.all(matches.map(match => replacement(match)));
  function replaceReducer(newStr, match, index) {
    return newStr.replace(match, substitutes[index]);
  }
  return matches.reduce(replaceReducer, str);
}

async function queryGit(config, name) {
  try {
    return execSync(`git config --get ${config}`, { encoding: 'utf8' }).trim();
  }
  catch (error) {
    console.error(`Template requires ${name}. But no ${name} was specified and
  I could not fetch it from the local git configuration`);
    error.type = 'NOTICE';
    throw error;
  }
}

async function fetchLicense(location) {
  return (await readFile(pathJoin(process.cwd(), location), 'utf8')).trim();
}

let pkg;

async function getPkgProperty(propChain) {
  if (!pkg) {
    pkg = (await readPkg()).packageJson;
  }
  const value = _get(pkg, propChain);
  if (!value) {
    const error = new Error(`Property "${propChain}" is not defined in package.json.`);
    error.type = 'NOTICE';
    throw error;
  }
  if (typeof value !== 'string') {
    console.warn(`  Warning: Property "${propChain}" in package.json is not a string. Its value will be coerced to ${value.toString()}`);
  }
  return value.toString();
}

async function buildRawHeader(settings) {
  async function getCustomProperty(propChain) {
    const value = _get(settings.custom, propChain);
    if (!value) {
      const error = new Error(`Custom property "${propChain}" is not defined in settings`);
      error.type = 'NOTICE';
      throw error;
    }
    if (typeof value !== 'string') {
      console.warn(`  Warning: Custom property "${propChain}" is not a string. Its value will be coerced to ${value.toString()}`);
    }
    return await findReplace(value.toString(), settings.queryPattern, replacer);
  }

  const replacers = {
    year() {
      const today = new Date();
      return today.getFullYear();
    },
    async author() {
      return (typeof settings.author === 'string') ?
        settings.author :
        await queryGit('user.name', 'author');
    },
    async contact() {
      return (typeof settings.contact === 'string') ?
        settings.contact :
        await queryGit('user.email', 'contact information');
    },
    async license() {
      let license;
      if (typeof settings.license.notice === 'string') {
        license = settings.license.notice;
      }
      else if (settings.license.location) {
        try {
          license = await fetchLicense(settings.license.location);
          // eslint-disable-next-line no-param-reassign
          settings.license.notice = license;
        }
        catch (error) {
          error.message = `Unable to read license from file
${error.message}`;
          error.type = 'NOTICE';
          throw error;
        }
      }
      else {
        const error = new Error('No license specified in settings');
        error.type = 'NOTICE';
        throw error;
      }
      return await findReplace(license, settings.queryPattern, replacer);
    },
    async spdx() {
      return (typeof settings.spdx === 'string') ?
        settings.spdx :
        await getPkgProperty('license');
      // TODO: check license ID strings against SPDX database
    },
    custom: getCustomProperty,
    pkg: getPkgProperty,
  };

  async function replacer(match) {
    const args = match.slice(2, -1).split('.');
    if (!replacers[args[0]]) {
      const error = new Error(`Unidentified pattern "${match}" in template`);
      error.type = 'NOTICE';
      throw error;
    }
    return await replacers[args.shift()](args);
  }

  return await findReplace(settings.template, settings.queryPattern, replacer);
}

export default buildRawHeader;
