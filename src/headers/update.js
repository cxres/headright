/*!
 *  Copyright (c) 2021, Rahul Gupta and Headright Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const nowYear = (new Date()).getFullYear();

function processDates(dates) {
  const prev = dates.slice(0, -4);
  const year = Number(dates.slice(-4));

  if (year === nowYear) {
    return `${prev}${nowYear}`;
  }
  else if (year === nowYear - 1) {
    if (prev.slice(-1) === '-') {
      return `${prev}${nowYear}`;
    }
    else {
      return `${prev}${year}-${nowYear}`;
    }
  }
  else {
    return `${prev}${year}, ${nowYear}`;
  }
}

function update(options) {
  return Object.freeze({
    from: options.match,
    to: function cp(match, ...args) {
      const groups = args.find(arg => typeof arg === 'object');
      if (groups) {
        if (groups.date) {
          const { date } = groups;
          return match.replace(date, processDates(date));
        }
      }
      return match;
    },
  });
}

export default update;
