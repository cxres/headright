/*!
 *  Copyright (c) 2021, Rahul Gupta and Headright Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import libUrl from 'url';
import { defaultLoaders } from 'cosmiconfig';

const esmLoader = async (filePath) => {
  try {
    const result = await import(libUrl.pathToFileURL(filePath));
    return result.default ? result.default : result;
  }
  catch (error) {
    console.error(`Failed to load configuration file ${filePath}`);
    throw error;
  }
};

const dualJsLoader = async (filePath) => {
  try {
    return defaultLoaders['.js'](filePath);
  }
  catch (error) {
    if (error.code === 'ERR_REQUIRE_ESM') {
      return esmLoader(filePath);
    }
    throw error;
  }
};

export {
  esmLoader as mjs,
  dualJsLoader as js,
};
