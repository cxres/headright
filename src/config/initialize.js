/*!
 *  Copyright (c) 2021, Rahul Gupta and Headright Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import { copyFile } from 'fs/promises';

async function initializeConfiguration(to) {
  return await copyFile(new URL('../../assets/default.js', import.meta.url), to);
}

export default initializeConfiguration;
