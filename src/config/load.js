/*!
 *  Copyright (c) 2021, Rahul Gupta and Headright Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import { cosmiconfig } from 'cosmiconfig';
import * as loaders from './loaders.js';

async function loadConfiguration(options = {}) {
  if (typeof options.name !== 'string') {
    throw new Error('No configuration file base name specified');
  }
  const {
    name,
    file,
  } = options;

  const title = options.title || `${name[0].toUpperCase}${name.substring(1)}`;

  const explorerConfig = {
    loaders: {
      '.mjs': loaders.mjs,
      '.js': loaders.js,
    },
    cache: false,
  };

  const defaultSearchPlaces = [
    `.${name}rc.cjs`,
    `.${name}rc.mjs`,
    `.${name}rc.js`,
    `${name}.config.cjs`,
    `${name}.config.mjs`,
    `${name}.config.js`,
  ];

  explorerConfig.searchPlaces = file ? [file] : defaultSearchPlaces;

  const explorer = cosmiconfig(name, explorerConfig);

  const result = await explorer.search();
  if (result === null) {
    throw new Error(`No ${title} configuration found.`);
  }
  if (typeof result.config !== 'object') {
    throw new Error(`Invalid ${title} configuration.`);
  }
  return result.config;
}

export default loadConfiguration;
