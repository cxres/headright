/*!
 *  Copyright (c) 2021, Rahul Gupta and Headright Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import replace from 'replace-in-file';

import update     from './headers/update.js';
import add        from './headers/add.js';
import getNotice  from './headers/get.js';

function noticeErrorMsg(message = '') {
  return `
  Error: Unable to build notice, shall not add new templates to files.
  ${message}`;
}

async function headright(options) {
  const {
    dry,
    files,
    config = {},
  } = options;

  const from = [];
  const to = [];

  let notice;
  try {
    notice = await getNotice(config.notice);
  }
  catch (error) {
    if (error.type === 'NOTICE') {
      console.error(noticeErrorMsg(error.message));
    }
    else {
      throw error;
    }
  }

  if (!options.updateOnly) {
    const addArgs = add(notice, config.add);
    from.push(addArgs.from);
    to.push(addArgs.to);
  }

  if (!options.addOnly) {
    const updateArgs = update(config.update);
    from.push(updateArgs.from);
    to.push(updateArgs.to);
  }

  return replace({
    dry,
    ignore: config.ignore,
    glob: {
      dot: true,
    },
    allowEmptyPaths: true,
    files,
    from,
    to,
  });
}

export default headright;
